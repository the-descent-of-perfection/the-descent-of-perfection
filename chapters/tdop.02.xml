    <chapter id="p1-c2_alchemyAsArt">
      <title>Alchemy as Art</title>

      <para>
	In the distance, Aran Mylo could see a man walking
	along the dirt road. He was one of the men from the forest,
	from the land of Kiowa Creek. He was wearing very
	little clothing, for the people from Kiowa Creek and
	other such lands were not subject to all local laws and
	certainly not many local customs. Just by looking at him,
	Aran Mylo could see that he had once been a very powerful man.
	He was old now, and perhaps weaker in the flesh, but his
	dignity and his earned respect preceded him as he walked.
      </para>

      <para>
	Aran Mylo leaned back in her chair and looked back over at her
	friend but Persis was gone. Had Persis really appeared to her
	from the future only to tell her that a shaman was
	approaching? Was the shaman dangerous? He didn't look it, but
	Aran Mylo decided to watch him closely nevertheless.
      </para>

      <para>
	&#34;Good evening, sir,&#34; the shaman said; he'd made good
	time on the road and was standing mere meters away now.
      </para>

      <para>
	Aran Mylo, still wondering where Persis had gotten off to,
	stammered a response, &#34;Are you the...wait --&#34;
      </para>

      <para>
	And then she came to her senses and remembered her
	manners, &#34;I'm sorry -- good evening.&#34;
      </para>

      <para> 
	The shaman smiled and bowed a little. &#34;I'm quite
      well, thank you. How are you, good sir?&#34;
      </para>

      <para>
	&#34;I'm fine, thank you. I think I was starting to drift off,
	though.&#34;
      </para>

      <para>
	The man nodded. &#34;I can see how you might do that on a day
	-- a night -- like this.  It's very peaceful, very
	silent.&#34;
      </para>

      <para>
	Aran Mylo nodded.  His words happened to have been her own
	thoughts.  She looked around and tried to think of something
	to say.  At last, she found herself wondering why the man was
	walking down that seldom-used dirt road when night had fallen.
	&#34;It's late in the evening and you're still walking? and
	there's no town nearby. Are you lost?&#34;
      </para>

      <para>
	The man smiled again, apparently appreciative of the monk's
	concern. &#34;No, I'm just travelling.&#34;
      </para>

      <para>
	&#34;Good.&#34; She was still watching him a little
	suspiciously, still wondering why Persis had forewarned
	her. Perhaps it hadn't been a cautionary warning, but a
	friendly alert. Maybe this man was important.
      </para>
      
      <para>
	She'd already guessed it, but for conversation's sake she
	asked, &#34;Where are you from?&#34;
      </para>

      <para>
	&#34;Kiowa Creek,&#34;
	the man said.
      </para>

      <para>
	&#34;I thought so,&#34; she said, immediately transitioning
	into the ancient Kiowa tongue. &#34;My
	mother was from Kiowa Creek.  I was raised there for most of
	my childhood.&#34;
      </para>

      <para>
	&#34;Then I'm glad I've stumbled across your home,&#34; the
	man said, also in his native tongue. &#34;I prefer to stay
	around my own people.&#34;
      </para>
      
      <para>
	&#34;Do you?&#34;
      </para>

      <para>
	The shaman nodded.
      </para>

      <para>
	&#34;My name's Aran
	Mylo.&#34;
      </para>

      <para>
	&#34;And I'm Marsa Mytra <footnote><para>MAR-suh
	MEET-ruh</para></footnote>.&#34;
      </para>

      <para>
	&#34;Glad to meet you,&#34; she
	said. He didn't say anything, for he had, in effect, already
	said that he was glad to meet her. Aran Mylo therefore
	continued: &#34;Well, Marsa Mytra, would you like some supper
	before you continue on your journey?&#34;
      </para>
      
      <para>
	&#34;I was hoping
	you'd offer, Aran Mylo. Being a monk, I suppose you're a
	vegetarian?&#34;
      </para>
      
      <para>
	&#34;Do you mind?&#34;
      </para>

      <para>
	&#34;Not at all.
	I'm a non-committed vegetarian myself.  When I'm in the mood,
	I'm a vegetarian but it's hard to be true to that practise all
	of the time.&#34;
      </para>

      <para>
	Aran Mylo, still trying to determine why Persis had told her
	of this man's arrival, continued to test the veracity of his
	story. &#34;Especially back home, I suppose,&#34; Aran Mylo
	said, listening next for some names of common dishes from home.
      </para>

      <para>
	&#34;I'm sure you, in your days at Kiowa
	Creek, ate many of the same meals I eat. Leaf-lamb, roasted
	night-feather, or maybe skewered pecthy.&#34;
      </para>

      <para>
	Aran Mylo
	stood from her seat. With a smile, she replied, &#34;Such a
	long time ago...&#34;
      </para>

      <para>
	Marsa Mystra followed the monk into the cottage.  The room was
	mostly brown, for all the wood that it contained.  There was
	not much furniture and most everything concerned her studies.
	Aran Mylo lit a number of candles with a gesture of her hand,
	and this cast upon the room dancing glows and
	strange shadows.
      </para>

      <para>
	  The traveller stood in the doorway for a
	long while, admiring all of the herbs and medicines on the
	shelves of the cottage. &#34;Are you a doctor as well as a
	monk?&#34;
      </para>

      <para>
	Aran Mylo loved it when people asked her about
	something she was truly interested in. And he had mentioned her
	work, which was one of her greatest interests.
	&#34;Actually,&#34; she said, forgetting all about food and
	drink, &#34;I'm an alchemist. I'm studying to become a
	Dyn-Alys.&#34;
      </para>

      <para>
	She could tell by his expression that he didn't know much of
	her Order. She explained: &#34;To be a Dyn-Alys, knowledge of herbs and medicine,
	nutrition, the body&#39;s humours and functions, the elements of the Earth, is
	required.  I don't just study these things because I want to
	become a Dyn-Alys, though.  I'm very interested in herbal
	medicines and nutrition.  I feel that most people forget how
	capable the body is if they only supply it with the proper
	ingredients of good health.&#34;
      </para>

      <para>
	Marsa Mytra said, &#34;I'm
	a doctor, myself.  Did I mention that already?&#34;
      </para>

      <para>
	&#34;Yes, I think so.&#34; Aran Mylo wasn't really sure if he
	had told her or if it had been Persis.
      </para>

      <para>
	&#34;I recognise most of these herbs...many of these seem to
	be from our own traditional medicines, in fact.&#34;
      </para>

      <para>
	&#34;Yes,&#34; Aran Mylo said almost eagerly, happy that
	someone had finally noticed her personal touch in her work.
	&#34;I use quite a bit of Kiowa Creek's traditional medicine
	in my magic and medicines.  That's foreign to many other
	Dyn-Alys.&#34;
      </para>

      <para>
	She thought about the subject for a few moments, then said,
	&#34;I suppose that's a way of personalising my work.  Almost
	an art, I think.&#34;
      </para>

      <para>
	&#34;The producer influences the
	product,&#34; Marsa Mytra said. &#34;I'm sure your mother would have
	been proud, young man, to see that you've not forgotten your
	roots.&#34;
      </para>
      
      <para>
	Aran Mylo smiled at what sounded like a pun, but in fact, she
	knew, was the Kiowan way of expressing a very real and deep
	connection to one's own past. The Kiowan considered an
	individual's past to be almost literally their roots, the way
	the plants have roots that bind that person to the
	ground. Aran Mylo, having moved away from her old childhood
	home, tended to differ greatly on this particular idea. She'd
	been summoned away from her roots, if she had any, and she'd
	been summoned by a force far too real to argue tradition
	with. 
      </para>

      <para>
	Just then, Aran Mylo remembered supper.  She quickly
	went over to the pot on the stove and stirred the vegetable
	and cous-cous stew. 
      <!--As she stirred the stew and prepared to
	serve it, she said, &#34;Since you'll be dining with me, you
	may as well know the truth of the matter.  I am a monk,
	but...&#34; Instead of telling the man herself, she just
	moved her hood away from her head to see if he would notice
	that she was a woman.  With her large robe on and most of her
	hair shaved off, she assumed that it may be difficult to tell
	whether she was a male or female. When combined with the
	rest of her head, however, her face was apparently feminine
	enough to reveal her little secret to a discerning (or perhaps
	expectant?) eye.  Marsa Mytra continued into the cottage with
	interest.  &#34;You're...&#34; &#34;Yes, I'm not a man.  I'm
	a woman...training to be a Dyn-Alys.&#34; &#34;And if
	someone found out?&#34; &#34;Well, if certain people found
	this out, I would be forbidden from continuing my studies.
	I'd probably even be exiled from this kingdom.  I
	figured that a fellow Medicine Man and Kiowan Creek
	probably wouldn't betray me.&#34; --> 
      </para>

      <para>
	She gave him a bowl of vegetable and cous-cous stew and a
	drink. &#34;Of course not,&#34; Marsa Mytra said, taking his
	supper from her.  He followed her to the small table near the
	window.  She sat down where she'd been before the evening had
	so suddenly come along; he sat across from her. &#34;How did
	you ever get started in this endeavour?...to become a
	Dyn-Alys, I mean.&#34; 
      </para>

      <para>
	Aran Mylo began eating.  She wasn't quick to answer, because
	she didn't like too many questions, even from a
	countryman. Questions led to idle conversation, and idle
	conversation led to little slips-of-tongue, like admitting
	that she was a woman in a man's role.
      </para>

      <para>
	Marsa Mytra was also chewing on some food, but he finished
	first, so he continued without her answer, &#34;I have a
	little confession, I must admit. I'm a shadowmage; I
	brought the night with me. And I didn't come to this cottage
	by accident. I&#39;ve travelled very far to speak with
	someone, perhaps your teacher?&#34;
      </para>

      <para>
	Aran Mylo took a drink, again to bide time. Shadowmages
	specialized in meteorology, in the atmospheric arts. When
	Marsa Mytra had said he'd brought the night, he really meant
	it; the evening had fallen prematurely in his wake. Someone
	who could have such an affect was not just a simple wanderer,
	or even just a local shaman. She was speaking with a great
	wizard, and he was either too humble or too laden with
	ulteriour motives to show it.
      </para>

       <para>
	She said, &#34;A very wise old Dyn-Alys <!--named Procyon
	Camelopardes--> began teaching me the arts and sciences of his
	Order. I was interested in the subject, and he was
	willing to teach me the basics because I was already teaching
	myself many of the basics without even realising that I
	was.
      </para>

      <para>
	&#34;I kept learning more and more until we both finally realised
	that I had become a very serious student of the Dyn-Alys
	Order. He hadn't intended to take on a pupil, as such, but
	that was what he had. So he inducted me. There were many
	reasons that he shouldn't have done it, but he did it nevertheless.&#34;
      </para>
	
      <para> 
	The shadowmage said, &#34;But alchemy is often said to be a study that
      cannot be accomplished in one lifetime.  He must have been a
      great teacher, for you to have learned so much.&#34;
      </para>

      <para>
	&#34;As it happens, he has said on
	numerous occasions that I am his best student. I'm flattered,
	of course, though I refuse to hear such words, as they tend to
	corrupt the thought process through pride. But he believed it,
	and he left, and I have not seen or heard from him in a very, very
	long time.&#34;
      </para>

      <para>
	Aran Mylo stood to clean her plate, and to depart from the
	conversation. The truth of it was that the art and science of
	Alchemy was a tradition that had been developed for centuries,
	and yet to take on alchemy was to start always from
	the beginning, as if though no such thing had ever
	existed. There was truly no Teacher of Alchemy, nor Alchemist texts,
	that would or could ever give much meaningful advice to a
	young student. A student could not very well accomplish what
	had been developed over centuries in just one short lifetime,
	so a student had to first work at extending his lifetime, then
	eliminating the boundaries altogether.
      </para>

      <para>
	Extension of life was relatively easy and had been perfected
	by the nutritionists and herbalists in the East. One only had
	to journey to the Orient for these secrets, which were readily
	shared with those who asked. Eliminating the limits of a
	lifetime, however, was at the very heart of Alchemy. Finding
	the purest truth -- the <emphasis>Elixir</emphasis> -- was of
	course the ultimate goal and had nothing to do with the
	physical lifespan, but instead with the Spiritual. 
      </para>
      
      <para>
	But first came the extension of the physical life so that the
	search for this Pure Truth could go on for as long as
	necessary. Marsa Mytra was a case-in-point; he had obviously
	grown old in his long life. He surely had many years ahead of
	him, if all went well, and was already mentally preparing for
	death. But death would come sooner or later and he probably,
	by this time, did not fear it. And yet how little he knew or
	understood of Alchemy. Even he, obviously a powerful
	shadowmage who could control the night and day, the sun and
	moon, the rain and clouds and winds, had no understanding of
	what Alchemy really meant.
      </para>

      <para>
	In truth, there were very few who did.
      </para>

      <para>
	Aran Mylo went to the open door and looked out over the
	rolling hills of her home. The fireflies were out, lighting up
	the night sky. They were out early, confused by the early
	night that the shadowmage had brought.
      </para>
    </chapter>
