<chapter id="p2c1_shadows">
<title>Shadows</title>

<para>
  There was a mountain range, usually known as The Rocks of Multahr
  Multahr, and it divided the largest border of the western kingdom
  from the Near East, which was officially Tyrean Lynx.
</para>

<para>
  The land there was a little stranger, with less meadowland and rolling
  green hill and more cliff and moutain. There were large bodies of
  waters beyond the Multahr Multahr mountain range, and in certain
  seasons there were great winds. Between waves and winds, land erosion
  of all sorts was more common than not. This jagged landscape combined
  with the frequency of clouds and storms gave the land a mystically
  evil reputation in the Western kingdom. 
</para>

<para>
  Pragmatically, the mountain range was cited by academics as the one
  literal barrier in the two kingdoms from engaging in all-out religious
  and political war. It was just too much trouble to move an entire army
  through the mountains, no matter how much greener the grass was on the
  other side, or how blasphemous the competing religion.
</para>

<para>
  At the foot of the mountain range, there was a small town famous for
  its hospitality to travelers.  The town, Hasan Cluster, had originally
  been named &#34;Hasan Cloister&#34; (or thereabouts) due to the mass
  of holy men who lived there. Somewhere in its recent history, someone
  had made a typographical error when placing it on a map and from then
  on the town was known as &#34;Hasan Cluster&#34;, and travellers
  unfamiliar with the region would always ask what exactly was clustered
  there. 
</para>

<para>
  It was all for the better, since Hasan Cluster was destined to be a
  traveller&#39;s town, located as it was near the easiest passages
  through the mountains and into the neighbouring kingdom. Though the
  kingdoms were very much opposed to one another, this rarely hindered
  practical business people from visiting one kingdom and then the
  other.
</para>

<para>
  Of course, the town also benefitted from the heavy traffic of
  diplomats and politicians. They were common visitors to the town and
  were usually appreciated since they always had far too much money to
  spend than practical. And for all its traffic, Hasan Cluster developed
  the reputation of being a tourist attraction even though there
  wasn&#39;t really much to see in the town. But it was the only safe
  town for those of the Tyree Lynx persuasion to visit their
  neighbouring kingdom without having to pretend that they were not from
  the Near East, so to many people from the East it was, for all
  practical purposes, The West.
</para>

<para>
  So prosperous was Hasan Cluster that a similar city had been
  established on the opposite side of the mountain range, where the
  passages entered the other kingdom. As true businessmen are wont to
  do, two men from either town met one day over lunch (as the story
  goes) and established a horse or mule exchange program. A traveller
  could pay one man for a horse, ride it across one of the mountain
  passages, and turn the horse over to the other man on the other side.
  From there, it would be rented again and taken back to the other side
  of the mountain range. Thus, off of one mule or horse, both men
  alternately made a fine profit for themselves. The forbidden goods
  that they smuggled back and forth with the unsuspecting riders didn't
  hurt profits, either.
</para>

<para>
  In Hasan Cluster, the grass soft and thick until the mountains, where
  the ground gave way to white and silver rocks, resembling the scales
  of a snake.
</para>

<para>
  There was a tavern in what was more or less the centre of Hasan
  Cluster.  Its name had been weathered off of its sign but everyone
  knew the tavern nevertheless. It was the unofficial welcoming centre
  of at least the town, at the most of the entire kingdom. It was to
  that tavern that Aran Mylo was directed by everyone familiar with the
  area, and it was easily found. Its most functional feature was a bar
  that was fully exposed to the street. Aran Mylo, having arrived at the
  tavern at dusk, decided to have a seat at the bar and have a drink of
  water or tea.
</para>

<para>
  As soon as she'd settled into her seat, Aran Mylo heard someone
  shouting. She did wonder why the bar was otherwise empty, so she
  turned to see if the man who was shouting was calling to her. When she
  turned around, she saw a young man sitting at an easel, presumably
  with a painting-in-progress before him. He was shouting for her to
  move; she was ruining his scene.
</para>

<para>
  Aran Mylo leapt from the barstool apologetically and wandered away
  from the bar, finding herself moments later amongst tables and chairs
  of the tavern&#39;s outdoor cafe.
</para>

<para>
  &#34;Good evening, father,&#34; said the man sitting at the table
  which Aran Mylo had come to stand against. He was dressed in nice
  clothes, he was moderately handsome for the times even though his
  hairline was receding. 
</para>

<para>
  Aran Mylo nodded politely at the man and started to choose a table of
  her own. 
</para>

<para>
  &#34;Just get into town?&#34; he asked.
</para>

<para>
  She nodded again.
</para>

<para>
  &#34;Headed East? Through the mountains?&#34;
</para>

<para>
  She nodded yet again.
</para>

<para>
  &#34;You should take a horse or a mule,&#34; he said. &#34;They rent
  them from the stables and you can drop them off in the town on the
  other side. It&#39;s a partnership. If you don&#39;t, the trip will be
  ten times as hard, two times as long, and a lot less safe.&#34;
</para>

<para>
  Aran Mylo considered the idea, nodding slowly.
</para>

<para>
  &#34;What&#39;s wrong?  Taken an oathe of silence?&#34; he asked.
</para>

<para>
  &#34;No sir,&#34; Aran Mylo said, sitting at the table next to his. &#34;I haven't spoken in so long, I
  must be out of the habit.&#34;
</para>

<para>
  &#34;Why haven't you spoken?&#34;
</para>

<para>
  &#34;I've been travelling,&#34; she explained. &#34;No one to talk to
  on the road.&#34;
</para>

<para>
  &#34;Horses.&#34; 
</para>

<para>
  &#34;Eh, what's that?&#34;
</para>

<para>
  &#34;They make good listeners.&#34;
</para>

<para>
  &#34;I've got a lot more time than I have money, so I think I&#39;ll
  probably just walk.&#34;
</para>

<para>
  The man took a sip of his coffee.  He leaned back in his chair and
  said, &#34;I appreciate your humility, father, but it&#39;s not
  smart. Or, it&#39;s not practical.&#34;     
</para>

<para>
  &#34;Practical,&#34; said Aran Mylo, &#34;Why do you say that?&#34; 
</para>

<para>
  &#34;You realise that when you&#39;re travelling East, there are a
  number of people heading West? There are a number of people from the
  East who do not appreciate the Tyrean Trehendil religion or its holy
  men. Way up there in those mountains - who would know if something
  happened to you?&#34;
</para>

<para>
  &#34;I&#39;ll travel carefully. Thanks for the tip.&#34;
</para>

<para>
  &#34;You&#39;d be smart to travel with a larger group heading
  East. You&#39;ll need a guide any way. I&#39;ll bet I could find
  someone going East, if you&#39;d like.&#34;
</para>

<para>
  He was a hard sell, so Aran Mylo laughed in an effort to change the
  subject. He must have detected it, because he said, &#34;There&#39;s
  no price. You're a holy man, it would be my distinct pleasure to be of
  assistance to you. I&#39;ll see if I can find someone going East who
  would be willing to take along a monk. Meet me here to-morrow morning
  at seven o&#39; clock.&#34;
</para>

<para>
  &#34;Seven o&#39; clock.&#34;
</para>

<para>
  He explained, &#34;This town is awake early.&#34;
</para>

<para>
  &#34;Seven is early?&#34;
</para>

<para>
  &#34;I&#39;ll see you to-morrow morning.&#34;
</para>

<para>
  &#34;Thank you,&#34; she said, and stood to leave. Aran Mylo had lost
  all interest in a drink at that point, deciding that getting away from
  the ardent businessman would be better than trying to find peace and
  quiet at the cafe.
</para>

<para>
  Off of a quieter street, she found a secluded alleyway which looked like
  it would serve well enough as a hotel room for the night. She laid out
  her blanket and sat down to meditate. In the distance,
  she could see Virgo hanging in the dark sky. She closed her eyes and
  the night seemed to last only three seconds. When she opened her eyes,
  it was morning and she could hear someone somewhere singing. It was
  a man's voice, a gentle tenor, probably up in his apartment or hotel
  room. 
</para>

<para>
  Aran Mylo listened for a few minutes, enjoying the morning air, and
  then stood and stuffed her blanket into her bag and went to
  have another look about town. She found a place to wash up behind a
  bakery, and while there the baker had stepped out to smell the fresh
  morning air. He saw the monk washing his face and was, as he
  would admit later, mildly surprised at such a sight.
</para>

<para>
  &#34;Hello,
  father.&#34; 
</para>

<para>
  &#34;Hello, baker,&#34; said Aran Mylo.
</para>

<para>
  &#34;I was just using this rain water to wash my face. I hope I haven&#39;t
  taken an improper liberty.&#34; 
</para>

<para>
  &#34;Not at all,&#34; said the baker.
  &#34;Would you like to come in? We have pastries fresh out of the oven.&#34;
</para>

<para>
  &#34;I&#39;m sorry, but I have an
  appointment to make at seven o&#39; clock.  Is it that time yet?&#34;
</para>

<para>
  He put his hands upon his fat belly and inhaled a deep breath.  He
  then answered: &#34;Not yet. You have ten minutes.  Let me get you a
  danish.&#34;
</para>

<para>
  &#34;I couldn&#39;t impose upon you like that.&#34; 
</para>

<para>
  He was already going back
  into the bakery, though, and in no time he was back at the doorstep
  with a danish. &#34;Thank you.&#34;
</para>

<para>
  &#34;My pleasure,&#34; said the baker. &#34;A fine morning, isn&#39;t
  it?&#34; 
</para>

<para>
  &#34;It is a very nice morning,&#34; said Aran Mylo. &#34;Is
  this sort of weather year-round?&#34; 
</para>

<para>
  &#34;Unfortunately, it is
  not. We have a stormy season; renegade clouds, I think,
  from across the mountains.&#34; 
</para>

<para>
  &#34;Thank you for the danish,&#34;
  said Aran Mylo. &#34;And the rain water. I must go and see if
  I&#39;ve found a guide for my journey East.&#34; 
</para>

<para>
  With that, Aran Mylo waved at the baker and hurried away to the tavern in the
  centre of the town, her danish in hand.  When she arrived at the cafe,
  she saw that she needn&#39;t have rushed after all, for no-one else
  was there. The painter who had yelled at her last night was there,
  unpacking his things and setting up his easle, this time looking not
  at the bar but down the street.
</para>

<para>
  Aran Mylo took a seat upon a barstool
  and began ate her danish.
</para>
</chapter>
