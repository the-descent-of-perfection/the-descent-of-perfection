<chapter id ="theTrial">
<title>The Trial</title>

<para>
  The very next day, in the early afternoon, guards came into the
  communal prison cell and sought out Aran Mylo. Since she'd stayed
  exactly where they'd left her, it took them no time at all to find
  her. They led her out of the cell, through the halls, and to a
  small room in which there was a barber.
</para>

<para>
  Aran Mylo was directed to the single stool in the room, next to
  which stood the barber, holding a razor blade in one hand and
  shaving lather in the other.  Aran Mylo at first wondered how Resmin
  ever managed to find a barber who would shave her head, but this
  question was answered when she noticed the chain around the poor
  man's ankle.  It was connected to the ankle of a large soldier, who
  held a sword in hand and wouldn't stop glaring menacingly at the
  barber.
</para>

<para>
  Aran Mylo took her seat
  and the barber began to lather her head. The barber set about his
  terrible work and it seemed to Aran Mylo that he was so ashamed of
  cutting off a woman's hair that his hands were trembling. And then
  he made that terrible first stroke, saying aloud as he did, &#34;God
  forgive me.&#34;
</para>

<para>
  He muttered the same plea with literally every stroke of the razor
  he made.
</para>

<para>
     When he'd finished shaving he cleaned the
     lather from her head with a towel, and massaged her scalp with
     mint and licorice oil.
</para>

<para>
  &#34;Thank you,&#34; said Aran Mylo at the end of it. To comfort the
  barber, she added, &#34;God bless you.&#34;
</para>

<para>
  The barber spat, and said, &#34;Heathen, I don't need the blessings
  of your wanton god.&#34;
</para>

<para>
  Her guards stepped forward and led her down some more halls. They
  passed the room in which she'd first met Resmin, then through a
  foyer of statues and paintings (not just a few were of Resmin), past
  a courtyard, and finally to a set of double doors.
</para>

<para>
  There, they waited a short while, until one of the doors opened
  and an official-looking woman with came into the hall. She looked
  at the monk. &#34;Aran Mylo?&#34;
</para>

<para>
  Aran Mylo nodded.
</para>

<para>
  The woman turned to
  the guards. &#34;You may go now.&#34;
</para>

<para>
  The guards departed and the woman
  said to the monk, &#34;The Resmin and her Court will be ready for you in
  a moment.&#34;
</para>

<para>
  Aran Mylo said nothing, and she was not expected to say
  anything. The woman remained there in the hall with her.
</para>

<para>
  In the silence of the hallway, Aran Mylo became aware that the woman
  was whispering. She turned to see who the woman was speaking with and
  saw that she was whispering to herself. After a few moments of
  watching the woman, Aran Mylo saw that she was counting to herself.
</para>

<para>
  At last, the woman reached four-hundred. She stopped counting and
  opened one of the doors. She prompted the monk inside and Aran Mylo
  stepped into Resmin&#39;s Court. She'd expected a throne room like the
  one her own Queen had in her palace, but Resmin's Court was small
  and simple. It was strong and secure, though, and the walls were
  of dark stone. There was not much light -- certainly no natural
  light -- and the ceiling was low, the architecture simple.  At the
  far end of the room there were steps leading to Resmin's single
  throne. On its left was an empty seat for the Palace&#39;s Dyn-Alys.
</para>

<para>
  Along
  the sides of the room, there were long tables filled with maps,
  charts, and open books. A number of monks stood near the table on
  the right, and a number of nobles near the table on the left.
  Resmin herself was sitting in her throne, listening to a woman
  standing beside her. 
</para>

<para>
  When Resmin noticed Aran Mylo, she turned back to the woman and
  pointed to the monk. She then motioned to Aran Mylo&#39;s new guide, the
  doorwoman, who led Aran Mylo forward to the throne.
</para>

<para>
  As they passed the tables, Aran Mylo could feel the eyes of the
  court upon her. They stopped a metre before the steps. It was
  strange to see Resmin in such formal clothing and in such a formal
  setting. Somehow, it almost seemed that Resmin looked more like a
  woman than she did before, even though she was wearing more clothes
  this time. It was due to her dress, probably; it was a dark dress,
  a white blouse underneath. A bit of her chest was exposed, but only
  enough to be ladylike. Even in her business attire, it was apparent
  that she had the body of an athlete, and this impressed Aran Mylo
  once again.
</para>

<para>
  Resmin straightened herself in her seat. The woman standing beside
  her took a step away. The Court came to order.
</para>

<para>
  Resmin said, more to the court than to the monk, "Aran Mylo, the
  charges brought against you by the Resmin -- me -- are that you
  trespassed in this holy kingdom and that you are clearly a spy. Your
  sentence is death. This sentence will be abolished if you can prove
  to this court that the charges are false."
</para>

<para>
  &#34;But Resmin, you know that I'm not a spy.  I explained
to you why I became a monk.&#34;
</para>

<para>
  &#34;That doesn't prove anything.  It only explains why you're a monk.&#34;
  Resmin motioned to the woman with whom she'd been speaking when Aran
  Mylo entered the room. &#34;This is my military advisor. She has been
  keeping a close eye on all of the borders of this kingdom because
  recently there have been a number of murders. Not just any sort of
  murder, of course; the ones I am concerned with deal with those
  people who are not often murdered, usually. You see, many of our
  monks and wizards and mystics and even alchemists have been
  disappearing or being killed. We suspect it's Tyree Trehendil foul
  play, of course.&#34;
</para>

<para>
  Aran Mylo was about to explain again why she was in the kingdom, but
  Resmin spoke before she could: &#34;In fact, we caught a woman dressed
  up as a monk just the other day.  You can imagine how suspiscious
  that was...turned out to be a prank, as I myself discovered
  yesterday. A ridiculous rumour that started in our holding cell, of
  all places.&#34;
</para>

<para>
  Aran Mylo at once realised that there were only two people in that court who
  knew that she was a woman: herself and Resmin. And Resmin had just
  blatantly given her a signal that it was not to be revealed. Could
  this mean that her primary judge was an ally? 
</para>

<para>
  &#34;How...strange...&#34; said Aran Mylo, secretly referring to her
  present situation and not to Resmin&#39;s story.
</para>

<para> 
  She heard a voice and turned to see who was talking. It was a
  monk dressed in black robes.  He was sitting at the end of one of the
  tables to Aran Mylo's right, having been busy looking over a book. As
  he leaned against the table, he said: &#34;The very idea of a woman
  monk is perverse.&#34;
</para>

<para> 
  Resmin turned to him and listened. He continued: &#34;First of
  all, what would be her idea of God?  Secondly, how would that affect
  her view of religion?  To me, there's always been a certain mystery of
  women and faith; do they worship God as a Divine Father or a Divine
  Mother? And if it's a Divine Father, then how do the opposite genders
  relate to one another?  How can one be sure that a woman worships God
  for His Divinity and not His Sexuallity?&#34;
</para>

<para>
  &#34;God is God,&#34; said Aran Mylo. &#34;Gender is corporeal.&#34;
</para>

<para>
  &#34;Then God is a neuter? a eunich, perhaps?&#34; the monk said, with a
  rude smile upon his face.
</para>

<para>
  &#34;Perhaps God cannot be fully understood by mortals such as
  ourselves,&#34; Aran Mylo said. &#34;Even those who God has spoken to.&#34;
</para>

<para>
  &#34;You're a student and you're telling me -- a Dyn-Alys, probably
  ten times older than you -- that I have a limited view of God?&#34; His
  words were volatile, but he was polite, more or less.  He looked up
  at Resmin.  &#34;Your highness, he's a fanatic.  A spy.&#34;
</para>

<para>
  &#34;A fanatic or a theologian?&#34; Resmin asked. &#34;I have a difficult
  time telling the two apart.&#34;
</para>

<para>
  None of the monks in the room laughed at her joke, but most of the
  politicians did. For the benefit of the court, she asked Aran Mylo, &#34;Why are you in my
  kingdom?&#34; 
</para>

<para>
  &#34;I'm looking for my teacher.&#34;
</para>

<para>
  &#34;Why would your teacher be here?&#34;
</para>

<para>
  The doors of the throne room opened and an old Dyn-Alys in white
  enetered, crossed the room, and travelled up
the stairs toward Resmin's throne.  He said nothing and moved quietly.
He took a seat in the small chair next to Resmin's throne; he was the
palace's Dyn-Alys.
</para>

<para>
  Aran Mylo said, &#34;He was always interested in
discussions and peace, so he may have come here to meet with members
of Tyree Lynx.&#34;
</para>

<para>
  Resmin suggested casually, &#34;He may have come here to murder them...&#34;
</para>

<para>
  &#34;Never. Not my teacher. All life to him is
precious; he doesn't even eat meat.&#34;
</para>

<para>
  &#34;Who is this great wizard?&#34; asked
Resmin.
</para>

<para>
  &#34;His name is Procyon Camelopardes,&#34; Aran Mylo said and
waited to see what sort of reaction his name would bring this time.
Resmin's reaction was only to turn to her
Dyn-Alys and raised her brow in question. The Dyn-Alys spoke to
Resmin but loud enough for the court to hear him: &#34;Procyon
Camelopardes is a name I've heard often, but I've never ever met
anyone with that name, nor have I ever met anyone claiming to have
ever met him...until now.  He's generally spoken of in the context of
radicalism, subversion.&#34;
</para>

<para>
  Resmin looked to Aran Mylo with a cocked eyebrow, waiting patiently
  for an explanation. Aran Mylo didn't have to provide one, however;
  the Dyn-Alys continued: &#34;What puzzles me is that I&#39;ve always heard
that he was Tyrean Lynx, not Tyrean Trehendil at all.&#34;
</para>

<para>
  Resmin shrugged. She turned back to Aran Mylo. &#34;Are there any Dyn-Alys in your own
kingdom who can give some credit to your claims?&#34;
</para>

<para>
  &#34;No-one really
knows me well back home, for I don't live in a monastery.&#34;
</para>

<para>
  &#34;Why
not?&#34; asked a monk along the right wall.
</para>

<para>
  &#34;For a number of
reasons.  I prefer solitude, I study diverse topics.&#34;
</para>

<para>
  &#34;What topics?&#34; the inquisitor asked.
</para>

<para>
  &#34;Alchemy,&#34; she said. The disgust that rippled through the
  monks in the room was palpable.  
</para>

<para>
  &#34;Alchemists are not to be trusted,&#34; the Dyn-Alys said to
  Resmin. &#34;They work in secret. We believe they may be working to
  amass wealth that could topple even the greatest queendom.&#34;
</para>

<para>
  &#34;Alchemy is a spiritual journey,&#34; Aran Mylo protested.
</para>

<para>
  Resmin didn't seem to care about those
sorts of things. She said, &#34;Can anyone give credit to your
explanations?&#34;
</para>

<para>
  &#34;The queen might.&#34;
</para>

<para>
  Resmin raised an eyebrow and said, &#34;I don't understand what you
  mean. I'm the Queen.&#34;
</para>

<para>
  There was nothing Aran Mylo could say to that, so she merely bowed
  in acceptance.
</para>

<para>
  &#34;Aran Mylo,&#34; said Resmin, &#34;why did your
teacher choose to teach <emphasis>you</emphasis>
wizardry? are you so special?&#34;
</para>

<para>
  Aran Mylo got the distinct feeling that Resmin was really asking
why her teacher chose to take on a female student. Resmin continued:
&#34;You must have really &#39;impressed&#39; him, or something.&#34;
</para>

<para>
  &#34;I must have, but I did so
without trying.  He saw promise in my herbal and gardening work.  I
was interested in religious studies.  It started out as casual
lessons, then I told him that I wished to dedicate my life to the
studies.&#34;
</para>

<para>
  The Dyn-Alys, unaware that Resmin was asking the
question because Aran Mylo was a woman, said, &#34;It's very common, your
highness, for one accomplished wizard to take on a student.  The
student does not necessarily have to be a member of an order or a
monastery when the training begins.&#34;
</para>

<para>
  Resmin nodded politely.  To
Aran Mylo, she said, &#34;If Procyon Camelopardes was found, he could
support your many claims?&#34;
</para>

<para>
  &#34;Yes, your highness,&#34; said Aran Mylo. &#34;But I haven't seen him in
years and I've begun to fear the worst. If it comforts you any, I
should let you all know that my own kingdom has been experiencing the
same problems that yours has. Our monks and wizards are also in
danger.  Most people there blame your kingdom.  Obviously, you blame
them.&#34;
</para>

<para>
  Resmin said, &#34;You must admit that the conclusion does seem
to be the most logical one. Our sects have yet to find a substantial
peace with one another.&#34;
</para>

<para>
  &#34;Unfortunately, you speak the truth,&#34;
said Aran Mylo.
</para>

<para>
  Resmin sat back in her throne. Her military
advisor and Dyn-Alys leaned closer to her. They held a quick and
quiet conference, then all stood or sat erect.
</para>

<para>
  Resmin said, &#34;In
spite of my court, I still find you intriguing, Aran Mylo. The
sentence is suspended for the time being. I haven't the time or money
to send out men in search of your wizard-teacher, so I'm not sure that
your story will ever be proven true.  Then again, neither will it be
proven false.&#34;
</para>

<para>
  With that, Aran Mylo
was rejoined by the doorwoman, who led her back to the foyer, where
two guards had come to take her back to the
prison cell.
</para>
</chapter>
