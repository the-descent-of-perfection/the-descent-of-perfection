<chapter id="dinnerWithTheQueen">
  <title>
    Dinner with the Queen
  </title>

<para>
  The next day passed quicker than the previous one, for Serpens Cauda
  kept Aran Mylo occupied. They talked of travelling, of religions,
  of astronomy and geology and alchemy, of philosophy and theology,
  gardening, music, and other little topics that both seemed to have
  even a passing interest in. 
</para>

<para>
  The day after that, Serpens Cauda still hadn&#39;t left Aran Mylo alone
  even once. They passed the morning away by talking of the same
  matters as they&#39;d discussed the day before. The afternoon was
  punctuated with a nap, taken alternately so that one could keep
  watch as the other slept. Having Serpens Cauda as a guard was like
  having a politician for a priest but it was the best that Aran Mylo
  could do at the time.
</para>

<para>
  On the third day, Aran Mylo noticed reflection of light upon the far
  wall, which could only mean one thing: someone from the palace was
  in the prison. Somehow she felt that it was to fetch her.
</para>

<para>
  Aran Mylo turned back toward her
companion, kicking her lightly to awaken her.
</para>

<para>
  &#34;What is it?&#34; Serpens Cauda asked, without moving and
  opening only one eye.
</para>

<para>
     &#34;A
torch,&#34; said Aran Mylo.
</para>

<para>
  Serpens Cauda sat bolt upright and, upon seeing the light for
  herself, shrank into the
shadows of the corner. &#34;Who?&#34;
</para>

<para>
  Aran Mylo shrugged. The cubicle lit up and a guard turned the
  corner. Soon, Serpens Cauda could not escape its light so she sat
  defiantly in the golden glow, waiting for the torch to pass by. The torch did
  not pass, however. The single guard, holding the torch in the air,
  came to a stop before Aran Mylo. He pointed to her and motioned for
  her to accompany him.
</para>

<para>
  With a nervous
  glance at the strange woman who was then her only friend, Aran Mylo
  stood. The guard turned and began up the walkway. Aran Mylo
  followed.  Serpens Cauda, watching them leave, whispered after the monk:
  &#34;I&#39;ll wait. Two days.&#34;
</para>

<para>
  Aran Mylo cleared the apprehension from her
  mind, knowing enough to know that it would do her no good to be nervous.  If
  this was her executioner, then so be it.  By the end of it all,
  she&#39;d meet God -- and in so doing Understand God -- and that was
  something she strived for any way. If this was not her executioner,
  then she had nothing to fear.
</para>

<para>
  A second guard in the hall opened the prison door for Aran Mylo and her
guide. The two guards then led Aran Mylo through the halls of the
castle, but this time they did not pass through the hall of
paintings. Instead, they turned to the right just before reaching that
hallway, and their journey ended at a small, intimate dining room.
</para>

<para>
  Along the walls of that room were three long tables with jars of
  wine and many candles upon them. Mostly nobles were seated along the
  tables, with the Resmin herself seated at the centre of the table at
  the far end of the room. A woman with glowing hair was seated to
  Resmin&#39;s right; apparently an important member of the court. There
  were a number of monks seated at the end of the table nearest the
  door, who glanced at Aran Mylo (and she at them) with slight
  interest; to a monk, meeting someone from another religion or sect
  is a small pleasure.
</para>

<para>
  Aran Mylo would have no time to discuss
  philosophy with them, however, for her seat was nearest the Resmin&#39;s
  seat. In fact, it was to the Resmin&#39;s immediate left. Aran Mylo
  was shown to her seat, and she sat in it. She&#39;d never before feasted
  with any nobility, and now she&#39;d dined with two opposing queens.
</para>

<para>
  Or had she really ever dined with the Queen of that far off kingdom?
  It seemed long ago, now. Long ago, and when she&#39;d still had Persia
  with her. A very long time ago indeed.
</para>

<para>
  This feast turned out to be one of wine and collection of rare foods
  from the north, where few vegetables tended to grow at all. These
  were roots, specifically, bathed in sherry and lit on fire before
  the diner&#39;s very eyes. A plate was given to Aran Mylo, and she was
  served a glass of red wine. 
</para>

<para>
  &#34;I&#39;m so glad you could make it, Aran Mylo. The guest room was comfortable,
 I hope? I was just telling them how you&#39;d come such a long way to
 visit me. All rested up now?&#34;
</para>

<para>
  At first Aran Mylo thought the queen had taken leave of her senses,
  but she quickly understood that they were playing a private
  game. Aran Mylo was a house guest. A visitor from afar, staying in a
  guest room there in the castle. Presumably that was a far more
  palatable story than her being a prisoner on trial for espionage.
</para>

<para>
  &#34;Yes, thank you,&#34; she said absently. She looked at the fiery
  root on her plate and said, &#34;I had a friend once, a long time
  ago, who ate fire.&#34;
</para>

<para>
  &#34;It&#39;s a delicacy here,&#34; Resmin said. &#34;Only, we wait for the fire
  to go out before taking the first bite.&#34;
</para>

<para>
  Resmin took a small silver pitcher and gave it to Aran
  Mylo, saying, &#34;Here&#39;s some Dew Sauce. It was gathered fresh from
  the gardens just
  this morning.&#34;
</para>

<para>
  Aran Mylo took the pitcher and did nothing with it.
</para>

<para>
  &#34;For the salad,&#34; a nobleman a few seats away said.
</para>

<para>
  Aran Mylo thanked him with a nod, and drizzled some of the sauce
  onto her salad. She tasted it and found that it had a marvelous
  flowery flavour, and when combined with the leafy greens in the
  bowl, made it seem like she was dining the most beautiful garden
  she could imagine.
</para>

<para>
  &#34;Herbs and foods were my first and true love,&#34; Aran Mylo said. &#34;Long
before I became an alchemist, I studied these subjects.&#34;
</para>

<para>
  Resmin smiled and lowered her voice, saying, &#34;You see, Aran Mylo?  That&#39;s
  why I doubt that you are a spy. You speak with just enough passion
  that you seem to be earnestly interested in the topics...yet never
  are you fanatical.&#34;
</para>

<para>
  &#34;Thank you. I think.&#34; Aran Mylo could smell the strong wine
  on Resmin&#39;s breath, and assumed the queen was on her third or fourth
  goblet by now.
</para>

<para>
  &#34;I noticed that before and at the trial.  When we spoke, there
  was honesty in your voice. When you were speaking with Virbena at
  the trial, <emphasis>he</emphasis> seemed to be more of a fanatic
  than you...and I think it was his intention to show that you were
  the fanatic.&#34;
</para>

<para>
  &#34;I&#39;m not a fanatic,&#34; Aran Mylo said. And she more or
  less believed that of herself.
</para>

<para>
  &#34;But take heed,&#34; Resmin said with a smile, &#34;no man would
  ever say that herbs and food were his first love. That is a woman
  speaking.&#34;
</para>

<para>
  Aran Mylo forced a smile because she could tell Resmin was making a
  joke. She asked softly, &#34;Does this mean that I&#39;m absolved?&#34;
</para>

<para>
  Resmin indicated with a motion of her hand and a move of her eyes
  that now was not the time for that discussion. Then she carried on as if
  they&#39;d been friends for years. &#34;&#34;I invited you to dinner because I needed company. Look
  around you. Look at all of these people.
</para>

<para>
  Aran Mylo looked, and in one glance saw exactly what the queen
  meant. Here was a woman who stripped off her gowns and robes and did
  acrobatics all afternoon, a woman who&#39;d found a friend in a strange
  monk accused of being a spy. Resmin was lonely, and perhaps a little
  bored.
</para>

<para> 
  &#34;I needed someone to talk to and you happen to be one of
  the more fascinating people I&#39;ve met in a long while.&#34;
</para>

<para>
  &#34;I&#39;ve never considered myself fascinating,&#34; Aran Mylo
  confessed.
</para>

<para> 
  &#34;Why are you fascinating? because you are a fanatic. That is
  what makes you interesting and everyone else boring. I don't know
  what you&#39;re doing here in my queendom. It's my queendom. My
  land. My people.&#34;
</para>

<para>
  Resmin halted for a moment, gathering her thoughts, and then
  continued, less drunkenly, &#34;You&#39;re not here for the reasons
  you claim, let us leave it at that. You are dangerous, you are a
  fanatic, and that excites us. All of us.
</para>

<para> &#34;You&#39;re a free person, it seems. You do what you want
to do, when you want. You defy all expectation, you are extremely
in-control of yourself. Your mind is as perfect as my body is perfect;
I respect that you have trained it thus. If only I had your mind or
you had my body, the resulting person would be a rare work of
art.&#34;
</para>

<para>
  &#34;I don&#39;t think I&#39;m as perfect as you may think I am, m&#39;lady.&#34;
</para>

<para>
  &#34;That would be difficult case to prove,&#34; said Resmin. &#34;I&#39;ve hired someone to play
music for us as we all eat.  Do you like music, Aran Mylo?&#34;
</para>

<para>
  &#34;Yes, it&#39;s very good for the digestion.&#34;
</para>

<para>
  &#34;Here he is now.&#34;
</para>

<para>
  Aran Mylo glanced up at the bard who&#39;d entered the room. To her
  surprise, it was the very same bard who had played at the castle in
  her homeland, and the same who she and Persis had met on the
  road. Or had they truly met him? Had that all been a dream, a
  vision, like perhaps even Persis herself had been?
</para>

<para>
  Something deep within her trembled, and she knew why.
</para>

<para>
  It had been that bard who had foretold of Persis&#39;s disappearance, of
  Aran Mylo&#39;s capture.
</para>

<para>
  &#34;That man is a harbinger,&#34; Aran Mylo said louder than she&#39;d
  intended. A few of the dinner guests turned toward her
  questioningly, but she ignored them all but Resmin. &#34;You have to
  send him away.&#34;
</para>

<para>
  &#34;I thought you said music was good for digestion,&#34; Resmin
  said.
</para>

<para>
  &#34;Not his.&#34;
</para>

<para>
  &#34;I will not send him away. I want music with my dinner,&#34;
  Resmin said.
</para>

<para>
  Aran Mylo all at once decided to take advantage of Resmin&#39;s
  intoxication and the harbinger of the bard to alter the course of
  her own story. She said, &#34;If I am a monk from a foreign land,
  and you have no reason to suspect me of the things my accusers told
  you, then pronounce me an ambassador and give me a guest room for
  the night. I&#39;ll be on my way in the morning.&#34;
</para>

<para>
  Resmin considered the monk&#39;s proposal. She pondered it for a few
  moments, and then a few more, and then said, &#34;You are of
  political interest here in my kingdom. I must admit, my entire
court is interested in your case and I cannot afford to make up
excuses on your behalf.&#34;
</para>

<para>
  The bard, playing his songs, gave Aran Mylo courage to
  persist. &#34;A mistake has been made.&#34;
</para>

<para>
  &#34;I&#39;m full of wine and food,&#34; said Resmin. &#34;Let&#39;s
  walk.&#34;
</para>

<para>
  She stood, motioning for Aran Mylo to join her, and then led the
  monk out to a balcony that looked over a terrible crevace in the
  Earth, and out at the mountain ranges of the east, into the dense
  clouds of the dusk sky. It was everything Aran Mylo had ever heard
  about the Eastern kingdom; dark and terrifying, savage and raw.
</para>

<para>
  Resmin sat in a chair and Aran Mylo sat nearby. Resmin pointed out
  to the horizon. &#34;The Hinge. Infested with dragons that you
  wouldn&#39;t believe. Long ago my people dared venture into that
  mountain range, seeking shelter from Western raiders. Few
  survived. This palace was erected on its borders in defiance.&#34;
</para>

<para>
  &#34;Are there still dragons there?&#34; Aran Mylo asked.
</para>

<para>
  Resmin nodded. &#34;But most of them are old now. There won&#39;t be any
  left, soon. And then someone will move into the mountains, and then
  his family, and then their friends, and then there will be cities
  there. Built within the skeleton of Xan-Li. I should be happy, but
  mostly it just makes me sad. When I was a little girl, there were
  dragons. And now there won&#39;t be any.&#34;
</para>

<!--para>
  &#34;It&#39;s only sad if you believe in it,&#34; Aran Mylo
  said. &#34;In some way, there&#39;ll always be dragons. As real for
  people as they were for you as a little girl. They just won&#39;t be in
  those mountains.&#34;
</para-->

<para>
  All night long, until the blue dawn, they would sit on that
  terrace. Resmin rested her feet upon the cross-legs of Aran Mylo&#39;s
  own chair. In a few hours, she and the monk were the only dinner guests that remained, for the
  others had either gone home or they had wandered off to some other
  part of the palace.
</para>

<para>
  &#34;I pronounce you an ambassador,&#34; Resmin said at
  last. &#34;It was a case of mistaken identity. You&#39;ll leave in the morning.&#34;
</para>

<para>
  &#34;Thank you,&#34; Aran Mylo said, not entirely as calmly as she&#39;d
  intended. &#34;You must know what that means to me.&#34;
</para>

<para>
  &#34;Whatever it is you&#39;re really looking for, I wish you luck,&#34;
  Resmin said. &#34;You know, you and I are more alike than you
realise, I think? And you&#39;re not so Tyrean Trehendil as you would like
to believe.&#34;
</para>

<para>
  &#34;I don&#39;t understand.&#34;
</para>

<para>
  &#34;Your conception of Tyree Lynx when you came here was, if I may
  guess, that it was brutal and unforgiving. Full of carnal energy and
  a lack of respect for God, and an over-reliance on man&#39;s laws. Am I
  correct? It&#39;s what they all say about us.&#34;
</para>

<para>
  &#34;That&#39;s not what I thought,&#34; Aran Mylo said, &#34;But it
  does have a reputation for being a savage&#39;s religion.&#34;
</para>

<para>
  &#34;But you&#39;ve seen that it is not,&#34; Resmin said. &#34;And in
  fact you yourself spurn manmade laws and force your own religion to
  fit to your own beliefs. You don&#39;t deny that?&#34;
</para>

<para>
  Aran Mylo wasn&#39;t about to argue with the woman who was giving her
  her freedom, but even so she couldn&#39;t honestly deny the question. It
  was self-evident that she was inventing her own rules.
</para>

<para>
  Resmin could see her acquiescence, and continued, &#34;This happens
  to some. They find the truth, and realize that it was to this truth
  they were bending their own beliefs. But why struggle to fashion
  your beliefs when the answer has been formed already?&#34;
</para>

<para>
  So that was it; Resmin was a zealot after all. She wanted, more than
  anything, to convert Aran Mylo. &#34;Yes, yes I see what you
  mean. I&#39;ve always admired Tyree
  Lynx and many of its concepts. In fact, I believe that our two
  sects would be much better off if they could find similarities and
  common ground rather than fighting over petty concerns.&#34;
</para>

<para>
  &#34;Promise me you&#39;ll think about it,&#34; Resmin said. &#34;I know
  of a few congregations that meet in private even within your own
  kingdom. Will you seek them out?&#34;
</para>

<para>
  &#34;Yes, yes, I&#39;ll do that,&#34; Aran Mylo said.
</para>

<para>
  &#34;You&#39;re not just saying that, are you?&#34;
</para>

<para>
  &#34;No, of course not,&#34; Aran Mylo said, and it hurt her to say
  it, because of course she was lying. She knew that Resmin&#39;s heart
  was singing at the words, and that hurt her even more.
</para>

<para>
  &#34;I just know you&#39;ll find so much happiness in the true word of
  the gods. I have.&#34; Resmin smiled, and then took from her neck
  one of the small silver necklaces that she&#39;d worn that evening. A
  pendant was on the chain; the image of some lesser god. &#34;You&#39;ll
  take this. It&#39;s my spiritual guide, and she&#39;ll be yours now. When
  you leave, you&#39;ll show it to the guards so they&#39;ll know you&#39;re an
  honoured guest of the Resmin.&#34;
</para>

<para>
  Aran Mylo took the pendant, putting it around her own neck, and
  smiled in gratitude. Truthfully, she wouldn&#39;t even know where to
  begin if she did seek out the congregations of rebellious
  worshippers in her kingdom. It wasn&#39;t that she feared or even
  rejected Tyree Lynx, but she wasn&#39;t the sort to become embroiled in
  religious disputes. She was an alchemist, above the need for
  structured religion and priesthoods. But for Resmin, her saviour,
  she&#39;d be Tyrean Lynx until, at least, she was well on the road.
</para>
</chapter>
