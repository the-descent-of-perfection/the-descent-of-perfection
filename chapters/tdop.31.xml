<chapter id="crystal">
<title>Crystal</title>

<para>
  Aran Mylo and Serpens Cauda couldn't help but run when they'd left
  the castle. In spite of her instincts that all was well, there was
  that nagging, persistent threat that the Resmin would awaken in
  the morning and demand that Aran Mylo be apprehended again, whether
  simply out of drunken regret or because the monk had taken the
  liberty of taking along a friend. So they ran.
</para>

<para>
  And they continued to run until they reached what Serpens Cauda considered safety: a
forest. As they continued into the forest, the trees became thicker
and this, for all practical purposes, protected them from the eyes of
others.
</para>

<para>
  It felt safer, and it felt more like home to Aran Mylo, who'd grown
  up in the forests of the west. Around noontime, they came upon a
  clearing in the forest where the shadows cast by the trees were
  interrupted by rays of sunlight shining down upon a large lake
  there. The lake's waters sparkled and rippled in the light, and to
  the far left there was even a small waterfall. There was a fallen
  tree, overgrown with moss and grass, forming a bridge over the lake,
  leading toward the small hill that formed a shallow portion of the
  lake and that emptied into the larger one via the fall. Along the
  rough banks of the lake was grass, and farther back there were the
  trees of the forest, reaching far over the clearing to form a slight
  canopy.
</para>

<para>
  The two women fell to the ground upon reaching the clearing and for
  the first time since they'd been imprisoned, let themselves relax
  and sink into the earth that they lay upon. The grass was thick and
  soft beneath them, and cool on their backs even as the sun warmed
  their faces. Aran Mylo opened her eyes and let them be dazzled by
  the maze of branches and leaves, and the clouds moving far above
  them.
</para>

<para>
  &#34;This is my private clearing,&#34; came Serpens Cauda's
  voice. &#34;My home.&#34;
</para>

<para>
  &#34;It's beautiful,&#34; said Aran Mylo. &#34;How did you find
  it?&#34;
</para>

<para>
  Serpens Cauda unbound her sandals and kicked them from her feet. She
  stood and approached the lake, kneeling by the waters and
washing her face with its crystal clear waters. She turned back to Aran
Mylo, saying, &#34;We're safe here. No evil can pass into this
clearing.&#34;
</para>

<para>
  &#34;It's magickal?&#34; Aran Mylo said, wondering if she sensed the
  magick or if she only had been taken by the place's beauty.
</para>

<para>
  &#34;We'll rest here for a while. Here, our minds are
free of concern. We're free.&#34;
</para>

<para>
  Aran Mylo folded her hands behind her head. If Serpens Cauda was
  telling the truth and the clearing was magickal, then that meant
  that Serpens Cauda herself was harmless. That made Aran Mylo
  happy. But even so, there were many questions about Serpens Cauda
  that hadn't been answered yet. And there was still the question of
  the bard and what he'd truly been a harbinger of this time.
</para>

<para>
  Aran Mylo pushed all of the questions from her mind and took a deep
  breath of the fresh air of the clearing and gazed back up at the
  sky. Her eyes began to
  close and she started to wonder about how long it had been
  since she'd truly slept. It seemed like a week. Longer, if she tried to
  think of a time when she'd peacefully slept. She crossed her feet
  and let her head fall to one side lazily, feeling a breeze tickle
  her as it scurried across her body.
</para>

<para>
  She heard the sound of water
  splashing, and she lifted her head to see what was happening. She
  saw that Serpens Cauda was still kneeling by the spring, but now
  there was another woman near her. This woman was in the water; she
  was naked and it did not look like she was treading water.
  Perhaps the waters were shallow enough for the woman to stand as she
  spoke with Serpens Cauda.  The woman glanced over at Aran Mylo and
  smiled, but by that time Aran Mylo's eyes were closing again.  She
  wondered if she'd just seen a mermaid, but dismissed the thought as
  she drifted off to the land of dreams.
</para>

<para>
  Aran Mylo dreamt that she was home in her cottage. As she lay in her
hammock by a window, she could see her garden outside. Her tomatoes
were looking very ripe, and there were a number of pea pods ready for
picking.
</para>

<para>
  And then the light from the window was obscured. Aran Mylo, in her
  dream, looked up to see what was blocking the sunlight, but whatever
  it had been was already gone.
</para>

<para>
  But there was a presence in her cottage now. She sat up, and there,
  in a corner of the room was the harlequin. &#34;We have to talk,
  Aran Mylo.&#34;
</para>

<para>
  Aran Mylo said, &#47;Yes.&#47;
</para>

<para>
  And Persis opened her mouth, but no sound came out. Aran Mylo
  struggled to hear, but all she could hear was breeze through
  treetops.
</para>

<para>
  Aran Mylo awakened to the sound of more splashing water, and
  playful laughter that seemed to echo within the clearing. The clearing, thankfully,
  had not been a dream and the day was as peaceful as the one in
  her dream. The sun was still high in the sky and she found that
  she was getting a little hot. 
</para>

<para>
  Aran Mylo sat up, rubbed her eyes, and looked
     about the clearing. In the glistening waters there were three
     women. When they swam, no feet left the water, but fins did.
     The women were swimming and playing like dolphins, propelling
     themselves up from the water so that they fell back onto
     the water with a great show of sparkles and mist.
</para>

<para>
  Aran Mylo had never seen a mermaid before and had never even been
  positive that they existed. Now she was, unless this was all a long
  and detailed dream. 
</para>

<para>
  Up on the raised hill of the clearing, from which
  the little waterfall descended, sat Serpens Cauda, naked, bathed in
  sunlight that glistened and bounced off of her wet skin the way
  stardust must have played with the great constellations. 
</para>

<para>
  For the first time, Aran Mylo noticed that the woman's hair was
  actually a dark green, tied back into a ponytail now with a flower
  and its stem. It was a sure sign of elven in her family lineage, and
  in many other respects Serpens Cauda exhibited elvish traits, and
  even her ears appeared slightly pointed now that Aran Mylo could see
  her in daylight.
</para>

<para>
Serpens Cauda did not appear to notice that Aran Mylo had awakened or
was now watching her. She was watching the mermaids at play, and
enjoying the water running along her body.
</para>

<para>
  Aran Mylo was soon on her feet, walking toward
   the fallen tree bridge. She couldn't take her eyes off of the
   mermaids, and she wanted to be near them. They spoke to one another
   in a foreign tongue that Aran Mylo imagined Serpens Cauda could
   perhaps understand.
</para>

<para>
  Aran Mylo reached the pond above
   the lake, where Serpens Cauda sat. The woman's body
   was thin from malnourishment, and it bore some scars and
   bruises. She still took no notice of Aran Mylo.
</para>

<para>
  &#34;I thought that water killed,&#34; Aran Mylo said, kneeling by
  her friend.
</para>

<para>
  Serpens Cauda said, &#34;Yes.&#34;
</para>

<para>
  And that was all she said.
</para>

<para>
  Aran Mylo sat next to the woman to watch the mermaids.
</para>

<para>
  &#34;Can you understand them?&#34; asked Aran Mylo.
</para>

<para>
  &#34;Right now they're curious about why you dreamed that I was a
  harlequin.&#34;
</para>

<para>
  &#34;How do you know what I was dreaming?&#34;
</para>

<para>
  &#34;I don't; they do. Mermaids can sense dreams.&#34;
</para>

<para>
  &#34;I didn't dream you were a harlequin. I dreamt of an old friend,
  who was a harlequin,&#34; said Aran Mylo. She felt like it was the
  first time she'd ever talked to anyone about Persis.
</para>

<para>
  Serpens Cauda listened to the mermaids, and then said, &#34;They say
  you are a spy.&#34;
</para>

<para>
  Aran Mylo said, &#34;Surely they are only sensing that I'm not a man
  as I claim to be.&#34;
</para>

<para>
  &#34;Maybe that's so,&#34; Serpens Cauda said. &#34;Disrobe and swim
  over to them. They can speak the common tongue.&#34;
</para>

<para>
  Aran Mylo removed her outer cloak and tossed it to shore. She stood
  to get into the lake, but Serpens Cauda took hold of her arm firmly,
  saying, &#34;Don't go to them clothed. They'll be insulted. They
  think clothes are brazen, and that people wear them to defy nature.&#34;
</para>

<para>
  Aran Mylo removed her undergarment, and underclothes, and naked
  stepped into the lake. The water was cold, but she waded in slowly,
  just far enough in to be covered in water. She couldn't swim, so she
  didn't dare go further.
</para>

<para>
  Serpens Cauda waded in behind her, and then swam past her. After
  swimming in circles for a short while, she asked the monk,
  &#34;Aren't you going to swim out?&#34;
</para>

<para>
  &#34;I don't know how to swim. I never learned.&#34;
</para>

<para>
  &#34;It's easy, I can teach you,&#34; Serpens Cauda said.
</para>

<para>
  Serpens Cauda swam over to where Aran Mylo was standing, and reached
  her hands out. Aran Mylo took them, hesitantly, and let Serpens
  Cauda pull her gently from her foothold. And in no time, she was
  swimming; or at least, she felt like she was. She was moving through
  the water, as if she herself was swimming.
</para>

<para>
  Serpens Cauda explained the movements to her, and moved her hands
  through the water. She was a good teacher, patient and gentle, and
  soon Aran Mylo really was swimming on her own. She wasn't very good
  at it, and tired easily because she was inefficient and regularly
  forgot to keep her fingers together. But she was swimming, and the
  mermaids were nearly within reach now.
</para>

<para>
  Aran Mylo noticed that the water had the effect of making Serpens
  Cauda's dark green hair appear even greener, more vibrant. And
  Serpens Cauda herself seemed to come to life in the water, as if it
  was a second home to her.
</para>

<para>
  &#34;Come down, it's easier under water!&#34; said Serpens Cauda, and
  at once bobbed to pull Aran Mylo under. The monk had no sooner taken
  a gulp of air than she was pulled under the water, dragged along by
  her friend's gentle but firm grip.
</para>

<para>
  Serpens Cauda led the monk on for a few meters, and then released
  her, and Aran Mylo found it natural to navigate without the
  cumbersome difference between air and water. She kicked her legs and
  pulled herself forward through the viscuous atmosphere, and she felt
  like she was flying. She soared over a completely new landscape,
  covered in coral and strange vegetation, inhabited by slippery fish
  that wove in and out and around little caverns that only they knew
  how to navigate.
</para>

<para>
  Aran Mylo's lungs started to burn, so she turned upward to swim for
  air. The surface, full of tumult and sparkles, seemed far away
  now. She was about to make a mad dash for it when she felt a hand
  grab hold of her arm. Aran Mylo looked to see what was the matter,
  and saw that Serpens Cauda was smiling confidently at her,
  indicating for her to pay close attention.
</para>

<para>
  Serpens Cauda took Aran Mylo's hand, and put it upon her own, and then
  she moved her hand to Aran Mylo's chest. She held it there and, it
  seemed, exhaled. Only she wasn't exhaling, at least not as Aran
  Mylo knew exhaling. Somehow, Serpens Cauda was exhaling <emphasis>into</emphasis> Aran
  Mylo, directly into her body. And she was exhaling breathable
  air. Aran Mylo could feel it filling her lungs, and she had to force
  herself not to physically inhale. It felt strange, to have
  her lungs filling with clean, fresh air without feeling the air
  travel through her nose and throat, but there it was nevertheless.
</para>

<para>
  Aran Mylo looked at Serpens Cauda with a million unspoken questions 
  resting on her lips. But she understood, so she nodded; when she needed
  air, she was to take Serpens Cauda's hand and put it to her chest. And,
  as if by magick, she would breathe without breathing.
</para>

<para>
  Serpens Cauda smiled, satisfied and happy, and swam on. She swam backwards, inviting Aran
  Mylo to follow. Aran Mylo needed no further prompting, and swam to
  pursue her sea friend. Some of the mermaids joined along the tour,
  and it seemed that they explored every corner of the lake for what
  must have been hours.
</para>

<para>
  Soon Aran Mylo's muscles grew tired of the strange exercise, and so
  she let herself float lazily, suspended by the foreign atmosphere,
  carried over the landscape by an unfelt current. Serpens Cauda, she
  realized, must have been half-mermaid. That certainly was the only
  explanation of being able to send air into someone else's lungs. And
  maybe it helped explain, at least a little, why she seemed so
  strange in general.
</para>

<para>
  Serpens Cauda appeared in Aran Mylo's view, her flesh blue under the
  water, reflecting water patterns from the nearby surface. She put
  her hand on Aran Mylo's chest for air, and then tugged at Aran Mylo
  to follow her.
</para>

<para>
  They swam together, away from the mermaids, into a small alcove that
  had been formed by the subterranean part of the fallen tree that served as the makeshift
  bridge over the surface of the lake. Aran Mylo's body, full of air
  as it was, kept wanting to float upward, but Serpens Cauda seemed to
  have no such inclination. Serpens Cauda sat, just as easily as one
  would sit in a chair above the surface, in the alcove, and she
  brought Aran Mylo close to her, wrapping her legs around the monk's
  waist so that she didn't float up and away.
</para>

<para>
  Serpens Cauda slowly, cautiously, put her hands to Aran Mylo's
  temples, touching them gently. Aran Mylo could feel the gradual,
  hesitant, shy approach of Serpens Cauda's very thoughts into her own
  consciousness.
</para>

<para>
  <emphasis>You&#39;re a mermaid!</emphasis> Aran Mylo thought,
  knowing that Serpens Cauda could hear her.
</para>

<para>
  <emphasis>My mother was a mermaid,</emphasis> Serpens Cauda
  thought. <emphasis>It was forbidden but she mated with a
  human.</emphasis>
</para>

<para>
  <emphasis>Water doesn't kill,</emphasis> Aran Mylo thought,
  <emphasis>Air does. That is what you meant to say.</emphasis>
</para>

<para>
  Serpens Cauda laughed, not just mentally. <emphasis>I get those
  concepts confused sometimes.</emphasis>
</para>

<para>
  <emphasis>Thank you for showing me this,</emphasis> Aran Mylo
  thought. <emphasis>I've never experienced anything so
  beautiful.</emphasis>
</para>

<para>
  <emphasis>Then stay here,</emphasis> Serpens Cauda thought, more
  timidly than Aran Mylo had ever heard her say anything before. She
  put her hand on Aran Mylo's chest again, ostensibly for air, but
  Aran Mylo could feel her own beating heart against Serpens Cauda's
  flesh and it felt strong and vibrant.
</para>

<para>
  <emphasis>I can&#39;t stay here,</emphasis> thought Aran
  Mylo. <emphasis>I'm not half mermaid like you, and I have my
  missions.</emphasis>
</para>

<para>
  <emphasis>You saw a harbinger at the palace,</emphasis> Serpens
  Cauda thought, concentrating hard now. <emphasis>One you'd seen
  before.</emphasis>
</para>

<para>
  <emphasis>Yes,</emphasis> Aran Mylo heard her own thoughts respond.
</para>

<para>
  <emphasis>Beware, he is the shadowmage,</emphasis> Serpens Cauda
  said. <emphasis>He's disguised
  himself with shadow magick, as they do. You must be careful; they
  already may suspect you.</emphasis>
</para>

<para>
  <emphasis>Marsa Mytra, the bard, the shadowmage,</emphasis> Aran
  Mylo's recollection flooded. But she blocked her thoughts
  afterwards; Serpens Cauda had, after all, known her dream. But now
  she understood why Persis had appeared to her back at her cottage
  before they'd even actually met, and then again in her dream there
  in the clearing. 
</para>

<para>
  <emphasis>I'm being impolite,</emphasis> Serpens Cauda thought, and
  Aran Mylo could feel her, mentally, backing away a little. Aran Mylo
  wrapped her legs around Serpens Cauda, as if she'd been moving away
  physically as well, and held her hand to her chest. And there they
  sat for a very long time, and in some reality, Aran Mylo knew, she
  did stay, right there with Serpens Cauda, and in another with
  Persis, and another in her own cottage alone, forever.
</para>
</chapter>